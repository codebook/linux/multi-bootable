# Multi-boot USB


* [multi-boot usb method](https://askubuntu.com/questions/572004/universal-usb-installer-cant-detect-usb/572023#572023)

* [Multiboot USB drive](https://wiki.archlinux.org/index.php/Multiboot_USB_drive)

* [Boot Multiple ISO from USB via Grub2 using Linux](https://www.pendrivelinux.com/boot-multiple-iso-from-usb-via-grub2-using-linux/)


* [YUMI – Multiboot USB Creator](https://www.pendrivelinux.com/yumi-multiboot-usb-creator/)



